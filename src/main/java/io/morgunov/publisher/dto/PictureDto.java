package io.morgunov.publisher.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;

@Builder
@Getter
public class PictureDto implements Serializable {
    private int width;
    private String fileName;
    private int userId;
}
