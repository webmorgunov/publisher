package io.morgunov.publisher.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class PictureContainerDto {
    private List<PictureDto> pictures;
}
