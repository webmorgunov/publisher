package io.morgunov.publisher.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "picture")
@Getter
@Setter
@Component
public class PictureProperties {

    private String filePath;
}
