package io.morgunov.publisher.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "picture-service")
@Getter
@Setter
@Component
public class PictureServiceProperties {

    private String getByUserIdUri;
}
