package io.morgunov.publisher.service;

import io.morgunov.publisher.dto.PictureContainerDto;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface PictureService {
    void resize(MultipartFile file, int width, int userId) throws IOException;
    PictureContainerDto getPicturesByUserId(int userId);
}
