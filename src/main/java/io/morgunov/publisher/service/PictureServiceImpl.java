package io.morgunov.publisher.service;

import io.morgunov.publisher.dto.PictureContainerDto;
import io.morgunov.publisher.properties.PictureProperties;
import io.morgunov.publisher.properties.PictureServiceProperties;
import io.morgunov.publisher.dto.PictureDto;
import io.morgunov.publisher.properties.RabbitMqProperties;
import lombok.AllArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

@Service
@AllArgsConstructor
public class PictureServiceImpl implements PictureService{

    private final RabbitTemplate rabbitTemplate;

    private final RestTemplate restTemplate;

    private final PictureServiceProperties pictureServiceProperties;

    private final RabbitMqProperties rabbitMqProperties;

    private final PictureProperties pictureProperties;

    @Override
    public void resize(MultipartFile file, int width, int userId) throws IOException {
        Path path = Paths.get(pictureProperties.getFilePath(), generateFileName(file.getOriginalFilename()));

        Files.createFile(path);
        Files.write(path, file.getBytes());

        PictureDto pictureDto = PictureDto.builder()
                .width(width)
                .fileName(path.getFileName().toString())
                .userId(userId)
                .build();
        rabbitTemplate.convertAndSend(rabbitMqProperties.getQueueName(), pictureDto);
    }

    @Override
    public PictureContainerDto getPicturesByUserId(int userId) {
        return restTemplate.getForObject(pictureServiceProperties.getGetByUserIdUri() + userId, PictureContainerDto.class);
    }

    private String generateFileName(String originFileName) {
        String extension = originFileName.substring(originFileName.lastIndexOf('.') + 1);
        return String.format("%s.%s", UUID.randomUUID().toString(), extension);
    }
}
