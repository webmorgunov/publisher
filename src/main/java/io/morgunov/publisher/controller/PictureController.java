package io.morgunov.publisher.controller;

import io.morgunov.publisher.dto.PictureContainerDto;
import io.morgunov.publisher.dto.PictureDto;
import io.morgunov.publisher.service.PictureService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/picture")
@AllArgsConstructor
public class PictureController {

    private final PictureService pictureService;

    @PostMapping("/resize")
    public ResponseEntity resize(@RequestParam MultipartFile file, @RequestParam Integer width, @RequestParam Integer userId) throws IOException {
        pictureService.resize(file, width, userId);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/getByUserId/{userId}")
    public ResponseEntity<PictureContainerDto> getPicturesByUserId(@PathVariable Integer userId) {
        return ResponseEntity.ok(pictureService.getPicturesByUserId(userId));
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity handleError() {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
}
